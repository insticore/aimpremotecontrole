package insticore.com.aimpremotecontrol.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.LinkedHashMap;

import insticore.com.aimpremotecontrol.R;
import insticore.com.aimpremotecontrol.containers.Track;
import insticore.com.aimpremotecontrol.adapters.PlaylistAdapter;
import insticore.com.aimpremotecontrol.containers.TrackInfoForUi;
import insticore.com.aimpremotecontrol.services.MusicService;
import insticore.com.aimpremotecontrol.controls.Controls;


public class PlaylistActivity extends Activity {

    private ListView playListView;
    private LinkedHashMap<Integer, Track> playlist = new LinkedHashMap<>();
    private ServiceConnection sConn;
    private Intent intent;
    private MusicService musicService;
    private boolean isBound = false;
    private int currentPosition = 0;

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case MusicService.NOTIFY_PLAYER_CLOSED:
                    finish();
                    break;
                case MusicService.NOTIFY_EXIT:
                    finish();
                    context.stopService(new Intent(context, MusicService.class));
                    break;
                case MusicService.NOTIFY_PLAYLIST_CHANGED:
                    LinkedHashMap<Integer, Track> newPlaylist = (LinkedHashMap<Integer, Track>) intent.getExtras().get("playlist");
                    ((PlaylistAdapter) playListView.getAdapter()).updateTracklist(newPlaylist);
                case MusicService.NOTIFY_TRACK_CHANGED:
                    currentPosition = musicService.getSession().getCurrentTrack().getPlaylistPosition();
                    playListView.post(new Runnable() {
                        @Override
                        public void run() {
                            playListView.setItemChecked(currentPosition, true);
                            playListView.setEnabled(true);
                        }
                    });
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);

        intent = new Intent(this, MusicService.class);
        playListView = (ListView) findViewById(R.id.playlistView);
        playListView.setOnItemClickListener(new OnPlaylistItemClick());
        playListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        playListView.setVisibility(View.INVISIBLE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(MusicService.NOTIFY_EXIT);
        filter.addAction(MusicService.NOTIFY_PLAYLIST_CHANGED);
        filter.addAction(MusicService.NOTIFY_TRACK_CHANGED);
        filter.addAction(MusicService.NOTIFY_PLAYER_CLOSED);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, filter);

        sConn = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                musicService = ((MusicService.MyBinder) binder).getService();
                currentPosition = musicService.getSession().getCurrentTrack().getPlaylistPosition();
                playlist = musicService.getSession().getPlaylist();
                final PlaylistAdapter adapter = new PlaylistAdapter(getApplicationContext(), playlist);
                playListView.setAdapter(adapter);
                playListView.post(new ScrollTask());
                isBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                isBound = false;
                finish();
            }
        };

    }

    /**
     * Task of selection of the current track in the list scrolling the list
     * so that the current track item would be in the middle
     *
     */
    private class ScrollTask implements Runnable {
        @Override
        public void run() {
            int padding = playListView.getHeight() / 2;
            playListView.setItemChecked(currentPosition, true);
            playListView.setSelectionFromTop(currentPosition, padding);
            playListView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        bindService(intent, sConn, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isBound)
            unbindService(sConn);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
        isBound = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isBound)
            unbindService(sConn);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
        isBound = false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * Listener for playlist item click. Sends a message to a music player
     * asking to play a track represented by the list item.
     */
    class OnPlaylistItemClick implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            playListView.setEnabled(false);
            Track track = (Track) playListView.getItemAtPosition(position);
            Controls.playTrack(track);
        }
    }
}
