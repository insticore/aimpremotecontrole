package insticore.com.aimpremotecontrol.containers;

import android.os.Parcel;
import android.os.Parcelable;

public class PlayingMode implements Parcelable {

    private boolean shuffle;
    private boolean repeat;

    /**
     * Constructs a playing mode with everything off
     */
    public PlayingMode() {
        shuffle = false;
        repeat = false;
    }

    /**
     * Constructs a playing mode with specified shuffling and repeating values
     * @param shuffle Is shuffling on
     * @param repeat Is repeating on
     */
    public PlayingMode(boolean shuffle, boolean repeat) {
        this.shuffle = shuffle;
        this.repeat = repeat;
    }

    /**
     * Checks if shuffling is on
     * @return true if shuffling is on
     */
    public boolean isShuffling() {
        return shuffle;
    }

    /**
     * Sets shuffling on/off
     * @param shuffle Is shuffling on
     */
    public void setShuffling(boolean shuffle) {
        this.shuffle = shuffle;
    }

    /**
     * Checks if repeating is on
     * @return true if repeating is on
     */
    public boolean isRepeating() {
        return repeat;
    }

    /**
     * Sets repeating on/off
     * @param repeat Is shuffling on
     */
    public void setRepeating(boolean repeat) {
        this.repeat = repeat;
    }

    /**
     * Constructs a playing mode from parsel
     * @param in
     */
    protected PlayingMode(Parcel in) {
        shuffle = in.readByte() != 0x00;
        repeat = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (shuffle ? 0x01 : 0x00));
        dest.writeByte((byte) (repeat ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PlayingMode> CREATOR = new Parcelable.Creator<PlayingMode>() {
        @Override
        public PlayingMode createFromParcel(Parcel in) {
            return new PlayingMode(in);
        }

        @Override
        public PlayingMode[] newArray(int size) {
            return new PlayingMode[size];
        }
    };
}