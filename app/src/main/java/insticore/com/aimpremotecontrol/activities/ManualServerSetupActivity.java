package insticore.com.aimpremotecontrol.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import insticore.com.aimpremotecontrol.R;
import insticore.com.aimpremotecontrol.services.MusicService;
import insticore.com.aimpremotecontrol.tools.IPScanner;
import insticore.com.aimpremotecontrol.tools.Tools;

/**
 * Activity that provides interface for manual input of AIMP server address (IP and port).
 * It also checks if provided address is correct and check if there is working AIMP server via this address
 */
public class ManualServerSetupActivity extends AppCompatActivity {

    private EditText ipEdit;
    private EditText portEdit;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_server_setup);
        ipEdit = (EditText) findViewById(R.id.ip);
        portEdit = (EditText) findViewById(R.id.port);

        PortWatcher portWatcher = new PortWatcher();
        portEdit.addTextChangedListener(portWatcher);
    }

    /**
     * Task of checking provided port number.
     * Sets the maximum possible port number if too large number was provided by user
     */
    protected class PortWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() > 0 && Integer.parseInt(editable.toString()) > IPScanner.PORT_NUMBER) {
                    portEdit.setText(String.valueOf(IPScanner.PORT_NUMBER));
                }
        }
    }

    /**
     * Listens for "Connect" button click.
     * It validates the IP address entered by user into the form and shows alert dialog if it is not correct
     * If it is correct AIMP plugin is checked at the address set by user.
     * If plugin is available the connection to AIMP is tried to be established.
     * If connection is successfully established a music service and main activity are started,
     * the manual server setup activity is closed and main server setup gets result OK.
     * If plugin is not available at the address or connection is failed alert dialog is shown.
     * @param v A view of "Connect" button
     */
    public void onConnectButtonListener(View v) {
        String host = ipEdit.getText().toString();
        if(!(Tools.ipValidator(host))) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ManualServerSetupActivity.this);
            builder.setTitle("Input error!")
                    .setMessage(host + " is not a valid IP address")
                    .setCancelable(false)
                    .setNegativeButton("ОК",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        }
        String port = portEdit.getText().toString();
        if (port.length() <= 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ManualServerSetupActivity.this);
            builder.setTitle("Input error!")
                    .setMessage("Please specify the port number")
                    .setCancelable(false)
                    .setNegativeButton("ОК",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        }
        String url = host + ":" + port;

        if (Tools.checkPlugin(url)) {
            Intent musicServiceIntent = new Intent(this, MusicService.class);
            musicServiceIntent.putExtra("url", url);
            startService(musicServiceIntent);
            Intent startActivityIntent = new Intent(ManualServerSetupActivity.this, MainActivity.class);
            startActivityIntent.putExtra("url", url);
            setResult(RESULT_OK, null);
            startActivity(startActivityIntent);
            finish();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ManualServerSetupActivity.this);
            builder.setTitle("Connection error!")
                    .setMessage("No AIMP control was found at " + url)
                    .setCancelable(false)
                    .setNegativeButton("ОК",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    /**
     * Listens for "Cancel" button click.
     * Destroys this activity
     * @param v A view of "Cancel" button
     */
    public void onCancelButtonListener(View v) {
        finish();
    }



}

