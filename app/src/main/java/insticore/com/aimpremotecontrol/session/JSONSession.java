package insticore.com.aimpremotecontrol.session;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.NoSuchElementException;

import insticore.com.aimpremotecontrol.containers.Address;
import insticore.com.aimpremotecontrol.containers.ControlPanelState;
import insticore.com.aimpremotecontrol.containers.PlayingMode;
import insticore.com.aimpremotecontrol.containers.Track;
import insticore.com.aimpremotecontrol.services.MusicService;
import insticore.com.aimpremotecontrol.tools.Tools;

public class JSONSession extends JSONSessionLight {
    private ControlPanelState controlPanelState = new ControlPanelState();
    private LinkedHashMap<Integer, Track> playlist = new LinkedHashMap<>();

    public JSONSession(String stringURL){
        super(stringURL);
    }

    public void askNextTrack() {
        String method = "PlayNext";
        sendRequest(method);
    }

    public void askPreviousTrack() {
        String method = "PlayPrevious";
        sendRequest(method);
    }

    public void askInfoOfCurrentTrack(){
        if (controlPanelState.getCurrentTrack().getId() < 0) {
                controlPanelState.getCurrentTrack().setArtist("Unknown");
                controlPanelState.getCurrentTrack().setTitle("Unknown");
                return;
        }
        String method = "GetPlaylistEntryInfo";
        HashMap <String,Object> params = new HashMap<>();
        params.put("track_id", controlPanelState.getCurrentTrack().getId());
        params.put("playlist_id", controlPanelState.getPlaylistId());
        JSONRPC2Response response = sendRequest(method, params);
       // Tools.printLine(response);
        HashMap<String, Object> result = new HashMap<>();
        try {
            result = new ObjectMapper().readValue(response.getResult().toString(), HashMap.class);
        } catch (Exception e) {
            return;
        }
        synchronized (controlPanelState) {
            controlPanelState.getCurrentTrack().setTitle((String) result.get("title"));
            controlPanelState.getCurrentTrack().setArtist((String) result.get("artist"));
            playlist.put(controlPanelState.getCurrentTrack().getId(), controlPanelState.getCurrentTrack());
        }
    }

    public void askToPlay() {
        String method = "Play";
        sendRequest(method);
       /* synchronized (controlPanelState) {
            controlPanelState.setPlaybackState("playing");
        }*/

    }
    public void askToPlay(int trackId) {
        String method = "Play";
        HashMap <String,Object> params = new HashMap<>();
        params.put("track_id", trackId);
        synchronized (controlPanelState) {
            params.put("playlist_id", controlPanelState.getPlaylistId());
        }
        sendRequest(method, params);
    }




    public void askToPause() {
        String method = "Pause";
        sendRequest(method);
       /* synchronized (controlPanelState) {
            controlPanelState.setPlaybackState("paused");
        }*/
    }

    public void getCover() {
        String method = "GetCover";
        HashMap <String,Object> params = new HashMap<>();
        synchronized (controlPanelState) {
            if (controlPanelState.getCurrentTrack().getId() < 0) {
                controlPanelState.setAlbumCover(ControlPanelState.defaultAlbumCover);
                return;
            }
        }
        params.put("track_id", controlPanelState.getCurrentTrack().getId());
        JSONRPC2Response response = sendRequest(method, params);
        HashMap<String, Object> result = new HashMap<>();
        if (response != null && response.indicatesSuccess()) {
            try {
                result = new ObjectMapper().readValue(response.getResult().toString(), HashMap.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            controlPanelState.setAlbumCover(ControlPanelState.defaultAlbumCover);
            return;
        }
        String url = Tools.addHttpToUrlString(serverUrl) + "/" + result.get("album_cover_uri").toString();
        try {
            Bitmap image = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
            controlPanelState.setAlbumCover(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static class Subscr {
        static String method = "SubscribeOnAIMPStateUpdateEvent";

        static HashMap <String,Object> cpsUpdateParams = new HashMap<>();
        static HashMap <String,Object> playlistUpdateParams = new HashMap<>();
        static {
            cpsUpdateParams.put("event", "control_panel_state_change");
            playlistUpdateParams.put("event", "playlists_content_change");
        }
    }

    public JSONRPC2Response subscribeOnUpdateControlPanelState() {
        return sendRequest(Subscr.method, Subscr.cpsUpdateParams);
    }

    public Boolean subscribeOnUpdatePlaylist() {
        JSONRPC2Response response = sendRequest(Subscr.method, Subscr.playlistUpdateParams);
        HashMap<String, Object> result = new HashMap<>();
        try {
            result = new ObjectMapper().readValue(response.getResult().toString(), HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Tools.printLine(response);
        Boolean tracklistChanged = (Boolean) result.get("playlists_changed");
        if (tracklistChanged == null) {
            return false;
        } else {
            return tracklistChanged;
        }
    }


    public void getPlaylistEntries() {
        Tools.printLine("Starting reading entries");
        String method = "GetPlaylistEntries";
        HashMap <String,Object> params = new HashMap<>();
        int playlistId = controlPanelState.getPlaylistId();
        params.put("playlist_id", playlistId);
        String[] fields = {"id", "title", "artist", "duration"};
        params.put("fields", fields);
        Tools.printLine("fields created");
        JSONRPC2Response response = sendRequest(method, params);
        Tools.printLine(response);
        HashMap<String, Object> result = new HashMap<>();
        try {
            result = new ObjectMapper().readValue(response.getResult().toString(), HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        List<List<Object>> resultList = (List<List<Object>>) result.get("entries");
        LinkedHashMap<Integer, Track> tempTrackList = new LinkedHashMap<>();

        if (resultList != null && !resultList.isEmpty()) {
            int tracklistPositionCounter = 0;
            for (List<Object> entry : resultList) {
                int trid = (int) entry.get(0);
                String title = (String) entry.get(1);
                String artist = (String) entry.get(2);
                int duration = (int) (((int) entry.get(3)) / 1000.0);
                tempTrackList.put(trid, new Track(trid, title, artist, duration, tracklistPositionCounter++));
            }
        }
        playlist = tempTrackList;

        synchronized (controlPanelState) {
            Track currentTrack = playlist.get(controlPanelState.getCurrentTrack().getId());
            if (currentTrack == null) {
                currentTrack = new Track();
            }
            controlPanelState.setCurrentTrack(currentTrack);
        }
    }


    public JSONRPC2Response sendRequest(String method) {
        List params = new ArrayList();
        params.add(new Object());
        int requestID = randomGenerator.nextInt(100000);
        JSONRPC2Request request = new JSONRPC2Request(method, params, requestID);
       // System.out.println(request.getID());
        try {
            return send(request);
        } catch (JSONRPC2SessionException e) {
            throw new NoSuchElementException("Can't get JSON response: " + e.getMessage());
        }
    }

    public JSONRPC2Response sendRequest(String method, HashMap<String, Object> params) {
        int requestID = randomGenerator.nextInt(100000);
        JSONRPC2Request request = new JSONRPC2Request(method, params, requestID);
        try {
            return send(request);
        } catch (JSONRPC2SessionException e) {
            throw new NoSuchElementException("Can't get JSON response: " + e.getMessage());
        }
    }

    public ControlPanelState getControlPanelState() {
        return controlPanelState;
    }


    public JSONRPC2Response askPlayerControlPanelState() {
       String method = "GetPlayerControlPanelState";
       try {
           return sendRequest(method);
       } catch (NoSuchElementException e) {
           return null;
       }
   }

    public MusicService.UpdateResult setControlPanelData(JSONRPC2Response response, MusicService.NeedToUpdate whatToUpdate) {
        if (response == null) {
            return MusicService.UpdateResult.Exit;
        }

        HashMap<String, Object> result = new HashMap<>();
        try {
            result = new ObjectMapper().readValue(response.getResult().toString(), HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        MusicService.UpdateResult uiUpdate;

        switch (whatToUpdate) {
            case Nothing:
                setFullControlPanelData(result);
                uiUpdate = MusicService.UpdateResult.Nothing;
                break;
            case TrackChanges:
                uiUpdate = setControlPanelDataForPlaylistChanged(result);
                break;
            case Everything:
                uiUpdate = setFullControlPanelData(result);
                break;
            case CpsChanges:
                uiUpdate = setControlPanelDataChanges(result);
                break;
            default:
                uiUpdate = MusicService.UpdateResult.Nothing;
                break;
        }
        return uiUpdate;
    }


    public MusicService.UpdateResult setFullControlPanelData(HashMap<String, Object> result) {
        synchronized (controlPanelState) {
            Integer trackId = (Integer) result.get("track_id");
            if (trackId == null)
                trackId = -1;
            getCurrentTrackInfoByTrackId(trackId, result);
            getPosition(result);
            getPlaybackState(result);
            Boolean shuffle = (Boolean) result.get("shuffle_mode_on");
            if (shuffle == null)
                shuffle = false;
            Boolean repeat = (Boolean) result.get("repeat_mode_on");
            if (repeat == null)
                repeat = false;
            controlPanelState.setPlayingMode(new PlayingMode(shuffle, repeat));

            return MusicService.UpdateResult.Everything;
        }
    }

    protected MusicService.UpdateResult setControlPanelDataForPlaylistChanged(HashMap<String, Object> result) {

        MusicService.UpdateResult update = MusicService.UpdateResult.Name;

        synchronized (controlPanelState) {
            Integer trackId = (Integer) result.get("track_id");
            if (trackId == null)
                trackId = -1;

            if (trackChanged(trackId)) {
                getCurrentTrackInfoByTrackId(trackId, result);
                getPosition(result);
                getPlaybackState(result);

                Boolean shuffle = (Boolean) result.get("shuffle_mode_on");
                if (shuffle == null)
                    shuffle = false;
                Boolean repeat = (Boolean) result.get("repeat_mode_on");
                if (repeat == null)
                    repeat = false;
                controlPanelState.setPlayingMode(new PlayingMode(shuffle, repeat));

                update = MusicService.UpdateResult.Track;
            } else {
                askInfoOfCurrentTrack();
            }
        }
        return update;
    }


    public MusicService.UpdateResult setControlPanelDataChanges(HashMap<String, Object> result) {

        Boolean exiting = (Boolean) result.get("aimp_app_is_exiting");
        if (exiting != null && exiting) {
            return MusicService.UpdateResult.Exit;
        }

        synchronized (controlPanelState) {
            MusicService.UpdateResult update = MusicService.UpdateResult.Position;

            getPosition(result);
            getPlaybackState(result);

            Boolean shuffle = (Boolean) result.get("shuffle_mode_on");
            if (shuffle == null)
                shuffle = false;
            Boolean repeat = (Boolean) result.get("repeat_mode_on");
            if (repeat == null)
                repeat = false;
            if (modeChanged(shuffle, repeat)) {
                controlPanelState.setPlayingMode(new PlayingMode(shuffle, repeat));
                update = MusicService.UpdateResult.PlayingMode;
            }

            Integer trackId = (Integer) result.get("track_id");
            if (trackId == null)
                trackId = -1;
            boolean trackChanged = trackChanged(trackId);
            if (trackChanged) {
                getCurrentTrackInfoByTrackId(trackId, result);
                update = MusicService.UpdateResult.Track;
            }

            return update;
        }
    }

    protected void getCurrentTrackInfoByTrackId(int trackId, HashMap<String, Object> result) {

        Integer playlistId = (Integer) result.get("playlist_id");
        Integer length = (Integer) result.get("track_length");

        setCurrentTrack(trackId, playlistId, length);
        getCover();
        askInfoOfCurrentTrack();
    }


    protected void getPosition (HashMap<String, Object> result) throws NoSuchElementException {
        Integer position = (Integer) result.get("track_position");
        if (position == null) {
            position = getPosition();
        }
        if (position == null) {
            position = 0;
        }
        controlPanelState.setPosition(position);
    }

    protected void getPlaybackState (HashMap<String, Object> result)  {
        String playbackState = (String) result.get("playback_state");
        if (playbackState != null && playbackState.equals("playing")) {
            controlPanelState.setPlaybackState(ControlPanelState.PlaybackState.Playing);
        } else {
            controlPanelState.setPlaybackState(ControlPanelState.PlaybackState.Paused);
        }
    }

    private void setCurrentTrack(Integer trackId, Integer playlistId, Integer length) {
        if (playlistId != null) {
            controlPanelState.setPlaylistId(playlistId);
        } else {
            controlPanelState.setPlaylistId(-1);
        }
        if (trackId == null) {
            controlPanelState.getCurrentTrack().setId(-1);
            return;
        } else {
            controlPanelState.getCurrentTrack().setId(trackId);
        }
        if (playlist.containsKey(trackId)) {
            controlPanelState.setCurrentTrack(playlist.get(trackId));
        } else {
            controlPanelState.setCurrentTrack(new Track());
            controlPanelState.getCurrentTrack().setId(trackId);
            if (length == null) {
                getLength();
            } else {
                controlPanelState.getCurrentTrack().setDuration(length);
            }
        }
    }

    private boolean trackChanged(Integer trackId) {
        int currentTrackId = controlPanelState.getCurrentTrack().getId();
        return currentTrackId != trackId;
    }

    private boolean modeChanged(Boolean shuffle, Boolean repeat) {
        PlayingMode currentMode = controlPanelState.getPlayingMode();
        return currentMode.isRepeating() != repeat || currentMode.isShuffling() != shuffle;
    }

    public void setShuffling(boolean shuffle) {
        int value = 0;
        if (shuffle)
            value = 1;
        String method = "Status";
        HashMap <String, Object> params = new HashMap<>();
        params.put("status_id", 41);
        params.put("value", value);
        sendRequest(method, params);

    }

    public void setRepeating(boolean repeat) {
        int value = 0;
        if (repeat)
            value = 1;
        String method = "Status";
        HashMap <String, Object> params = new HashMap<>();
        params.put("status_id", 29);
        params.put("value", value);
        sendRequest(method, params);
    }


    public void setPosition(int position) {
        if ((position + 1) >= controlPanelState.getCurrentTrack().getDuration()) {
            position++;
        }
        System.out.println("Position: " + position);
        String method = "Status";
        HashMap <String, Object> params = new HashMap<>();
        params.put("status_id", 31);
        params.put("value", position);
        sendRequest(method, params);
        //askPlayerControlPanelState();

    }
    public void getLength() {
        String method = "Status";
        HashMap <String, Object> params = new HashMap<>();
        params.put("status_id", 32);
        JSONRPC2Response response = sendRequest(method, params);
        HashMap<String, Object> result = new HashMap<>();
        try {
            result = new ObjectMapper().readValue(response.getResult().toString(), HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        controlPanelState.getCurrentTrack().setDuration((Integer) result.get("value"));
    }


    public Integer getPosition() {
        String method = "Status";
        HashMap <String, Object> params = new HashMap<>();
        params.put("status_id", 31);
        JSONRPC2Response response = sendRequest(method, params);
        HashMap<String, Object> result = new HashMap<>();
        try {
            result = new ObjectMapper().readValue(response.getResult().toString(), HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return (Integer) result.get("value");
    }

    public String deleteTrack() {
        String method = "RemoveTrack";
        HashMap <String, Object> params = new HashMap<>();
        params.put("track_id", controlPanelState.getCurrentTrack().getId());
        params.put("playlist_id", controlPanelState.getPlaylistId());
        params.put("physically", true);
        JSONRPC2Response response = sendRequest(method, params);
        if (!response.indicatesSuccess()) {
            String message = response.getError().getMessage();
            System.out.println(message);
            return message;
        }
        return "Track deleted";
    }

    public LinkedHashMap<Integer, Track> getPlaylist() {
        return playlist;
    }

    public Track getCurrentTrack() {
        return controlPanelState.getCurrentTrack();
    }

}