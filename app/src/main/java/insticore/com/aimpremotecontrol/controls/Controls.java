package insticore.com.aimpremotecontrol.controls;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.content.LocalBroadcastManager;

import java.util.LinkedHashMap;

import insticore.com.aimpremotecontrol.containers.ControlPanelState;
import insticore.com.aimpremotecontrol.containers.PlayingMode;
import insticore.com.aimpremotecontrol.containers.Track;
import insticore.com.aimpremotecontrol.application.AimpControlApp;
import insticore.com.aimpremotecontrol.containers.TrackInfoForUi;
import insticore.com.aimpremotecontrol.services.MusicService;

public class Controls {
    public static void playControl() {
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(new Intent(MusicService.NOTIFY_PLAY_PAUSE));
    }

    public static void nextControl() {
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(new Intent(MusicService.NOTIFY_NEXT));
    }

    public static void previousControl() {
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(new Intent(MusicService.NOTIFY_PREVIOUS));
    }
    public static void positionControl(int position) {
        Intent intent = new Intent(MusicService.NOTIFY_POSITION);
        intent.putExtra("position", position);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);

    }
    public static void deleteTrack() {
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(new Intent(MusicService.NOTIFY_DELETE));
    }
    public static void showToast(String message) {
        Intent intent = new Intent(MusicService.NOTIFY_SHOW_TOAST);
        intent.putExtra("toast", message);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);

    }

    public static void playTrack(Track track) {
        Intent intent = new Intent(MusicService.NOTIFY_PLAY_TRACK);
        intent.putExtra("track", track);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);

    }

    public static void needControlPanelState() {
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(new Intent(MusicService.NOTIFY_NEED_CPS));
    }

    public static void needCover() {
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(new Intent(MusicService.NOTIFY_NEED_COVER));
    }

    public static void updatePlaylist(LinkedHashMap<Integer, Track> newPlaylist) {
        Intent intent = new Intent(MusicService.NOTIFY_PLAYLIST_CHANGED);
        intent.putExtra("playlist", newPlaylist);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);
    }

    public static void trackChanged(TrackInfoForUi info) {
        Intent intent = new Intent(MusicService.NOTIFY_TRACK_CHANGED);
        intent.putExtra("track_info", info);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);
    }

    public static void coverChanged(Bitmap cover) {
        Intent intent = new Intent(MusicService.NOTIFY_COVER_CHANGED);
        intent.putExtra("cover", cover);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);
    }

    public static void modeChanged(PlayingMode mode) {
        Intent intent = new Intent(MusicService.NOTIFY_PLAYING_MODE_CHANGED);
        intent.putExtra("mode", mode);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);
    }


    public static void positionChanged(int position) {
        Intent intent = new Intent(MusicService.NOTIFY_POSITION_CHANGED);
        intent.putExtra("position", position);

        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);

    }

    public static void playbackStateChanged(ControlPanelState.PlaybackState playbackState) {
        Intent intent = new Intent(MusicService.NOTIFY_PLAYBACK_STATE_CHANGED);
        intent.putExtra("state", playbackState);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);
    }

    public static void exit() {
        AimpControlApp.getContext().stopService(new Intent(AimpControlApp.getContext(), MusicService.class));
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(new Intent(MusicService.NOTIFY_EXIT));
    }

    public static void setShuffle(boolean shuffle) {
        Intent intent = new Intent(MusicService.NOTIFY_SHUFFLE);
        intent.putExtra("shuffle", shuffle);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);
    }

    public static void setRepeat(boolean repeat) {
        Intent intent = new Intent(MusicService.NOTIFY_REPEAT);
        intent.putExtra("repeat", repeat);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);
    }

    public static void aimpClosed(String url) {
        Intent intent = new Intent(MusicService.NOTIFY_PLAYER_CLOSED);
        intent.putExtra("url", url);
        LocalBroadcastManager.getInstance(AimpControlApp.getContext()).sendBroadcast(intent);
    }

}