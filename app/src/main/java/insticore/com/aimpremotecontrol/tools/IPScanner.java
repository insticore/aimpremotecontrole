package insticore.com.aimpremotecontrol.tools;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import insticore.com.aimpremotecontrol.containers.Address;
import insticore.com.aimpremotecontrol.controls.Handlers;


public class IPScanner {
    ArrayList<Address> list;
    private ExecutorService iPsExecutor = Executors.newCachedThreadPool();
    private ExecutorService portsExecutor = Executors.newCachedThreadPool();
    private List<Future<?>> futureIPList = new ArrayList<>();

    private volatile boolean done = false;
    private Object synchr = new Object();

    public final static int PORT_NUMBER = 65535;
    public final static int DEFAULT_PORT = 3333;

    public IPScanner() {
        list = new ArrayList<>();
    }

    public IPScanner(ArrayList<Address> list) {
        this.list = list;
    }

    public void getOpenServers() {
        list.clear();
        synchronized (Handlers.SERVERS_LIST_CHANGED_HANDLER) {
            Handlers.SERVERS_LIST_CHANGED_HANDLER.sendMessage(Handlers.SERVERS_LIST_CHANGED_HANDLER.obtainMessage());
        }
        String localAddress = "";
        String localIpAddress = getLocalIpAddress();
        if (localIpAddress == null) {
            return;
        }
        String[] tokens = localIpAddress.split("\\.");
        for (int i = 0; i < 3; i++)
            localAddress += tokens[i] + ".";
        Runnable[] ipCheckers = new Runnable[251];
        for (int i = 0, lastOct = 2; i < 251; i++, lastOct++) {

            InetAddress ip = null;
            try {
                ip = InetAddress.getByName(localAddress + lastOct);
            } catch (UnknownHostException e) {
                e.printStackTrace();
                continue;
            }
            if (done)
                return;
            ipCheckers[i] = new IPRunner(ip);
        }
        for (int i = 0; i < 251; i++) {
                futureIPList.add(iPsExecutor.submit(ipCheckers[i]));
        }
        iPsExecutor.shutdown();
        while (!iPsExecutor.isTerminated()){}
        portsExecutor.shutdown();
        while (!portsExecutor.isTerminated()){}
        System.out.println("Search is OVER");
    }

    public void setDone() {
        iPsExecutor.shutdownNow();
        portsExecutor.shutdownNow();
        try {
            iPsExecutor.awaitTermination(3, TimeUnit.SECONDS);
            portsExecutor.awaitTermination(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private class PortRunner implements Runnable {
        InetAddress ip;
        int port;
        private volatile boolean stop = false;
        public PortRunner(InetAddress ip, int port) {
            this.ip = ip;
            this.port = port;
        }

        public void run() {
                if (stop)
                    return;
                if (!isOpen()) {
                    return;
                }
                Address address = new Address(ip.getHostAddress(), String.valueOf(port), ip.getHostName());
                boolean pluginIsWorking = Tools.checkPlugin(address);

            if (stop)
                return;

                if (pluginIsWorking) {
                    synchronized (list) {
                        list.add(address);
                    }
                    synchronized (Handlers.SERVERS_LIST_CHANGED_HANDLER) {
                        Handlers.SERVERS_LIST_CHANGED_HANDLER.sendMessage(Handlers.SERVERS_LIST_CHANGED_HANDLER.obtainMessage());
                    }
                    System.out.println(address.toString() + "found");
                }

        }

        public boolean isOpen() {
            Socket socket = null;
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(ip, port), 150); //300 milliseconds tested
                System.out.println(ip + ":" + port + " is open");
                return true;
            } catch (IOException e) {
                System.out.println(ip + ":" + port  + " is closed");
                return false;
            } finally {
                if(socket != null)
                    try {
                        socket.close();
                    }
                    catch(Exception e) {
                        System.err.println(e.getMessage());
                    }
            }
        }
    }


    private class IPRunner implements Runnable {
        InetAddress host;
        Process process;
        private volatile boolean stop = false;
        private  List<Future<?>> futurePortList = new ArrayList<>();



        public IPRunner(InetAddress ip) {
            this.host = ip;
        }

        public void run() {
            if (pingHostInLan(host.getHostAddress())) {
                futurePortList.add(portsExecutor.submit(new PortRunner(host, DEFAULT_PORT)));
                for (int port = 1; !Thread.currentThread().isInterrupted() && port < DEFAULT_PORT; port++) {
                    futurePortList.add(portsExecutor.submit(new PortRunner(host, port)));
                }
                for (int port = DEFAULT_PORT + 1; !Thread.currentThread().isInterrupted() && port <= PORT_NUMBER; port++) {
                    futurePortList.add(portsExecutor.submit(new PortRunner(host, port)));
                }
            }
        }


        private boolean pingHostInLan (String host) {
            String result = null;
            try {
                String[] cmd = {
                        "sh",
                        "-c",
                        "if ping -c 2 -i 0.3 " + host + " > /dev/null; then echo true; else echo false; fi"
                };
                ;
                synchronized (synchr) {
                    process = Runtime.getRuntime().exec(cmd);
                }
                while (true) {
                    if (Thread.currentThread().isInterrupted()) {
                        process.destroy();
                        return false;
                    }
                    try {
                        process.waitFor();
                        }  catch (InterruptedException e) {
                            continue;
                    }
                break;
                }

                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(process.getInputStream()));
                StringBuilder output = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null)
                    output.append(line);
                process.destroy();
                reader.close();
                result = output.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (result != null) {
                if (result.equals("true")) {
                    System.out.println("Found reachable host: " + host);
                    return true;
                }
            }
            return false;
        }
    }

}
