package insticore.com.aimpremotecontrol.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.Display;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import insticore.com.aimpremotecontrol.containers.ControlPanelState;
import insticore.com.aimpremotecontrol.containers.PlayingMode;
import insticore.com.aimpremotecontrol.R;
import insticore.com.aimpremotecontrol.containers.TrackInfoForUi;
import insticore.com.aimpremotecontrol.services.MusicService;
import insticore.com.aimpremotecontrol.controls.Controls;

/**
 * Main activity of the application.
 * It shows all information about current track, provides player and application controls
 */
public class MainActivity extends AppCompatActivity {
    private TextView titleTextView;
    private TextView artistTextView;
    private SeekBar progressBar;
    private ImageView cover;
    private ImageButton playPauseButton;
    private Timer timing = new Timer();
    private ProgressDialog progressDialog;
    private RelativeLayout coverLayout;
    private TextView timeTextView;
    private TextView durationTextView;
    private ImageButton shuffleButton;
    private ImageButton repeatButton;



    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case MusicService.NOTIFY_PLAYER_CLOSED:
                    stopTimer();
                    String url = intent.getExtras().getString("url");
                    prepareClosedDialog(url);
                    break;

                case MusicService.NOTIFY_PLAYBACK_STATE_CHANGED:
                    new updatePlaybackTask().execute(intent);
                    break;
                case MusicService.NOTIFY_POSITION_CHANGED:
                    new updatePositionTask().execute(intent);
                    break;
                case MusicService.NOTIFY_TRACK_CHANGED:

                    new updateTrackTask().execute(intent);
                    break;
                case MusicService.NOTIFY_COVER_CHANGED:
                    new updateCoverTask().execute(intent);
                    break;
                case MusicService.NOTIFY_EXIT:
                    finish();
                    break;
                case MusicService.NOTIFY_SHOW_TOAST:
                    String message = intent.getExtras().getString("toast");
                    int duration = Toast.LENGTH_SHORT;
                    Toast.makeText(getApplicationContext(), message, duration).show();
                    break;
                case MusicService.NOTIFY_PLAYING_MODE_CHANGED:
                    new updatePlayingModeTask().execute(intent);
                    break;
            }
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("AIMP Remote Control");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        progressBar = (SeekBar) findViewById(R.id.progressBar);
        progressBar.incrementProgressBy(1);
        progressBar.setOnSeekBarChangeListener(new ProgressListener());
        progressBar.bringToFront();

        titleTextView = (TextView) findViewById(R.id.titleText);
        artistTextView = (TextView) findViewById(R.id.artistText);
        timeTextView = (TextView) findViewById(R.id.current_time_label);
        durationTextView = (TextView) findViewById(R.id.duration_label);
        timeTextView.setText(R.string.zero_time);
        durationTextView.setText(R.string.zero_time);

        cover = (ImageView) findViewById(R.id.albumCover);
        coverLayout  = (RelativeLayout) findViewById(R.id.cover_layout);

        coverLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int width  = coverLayout.getMeasuredWidth();
                int height = coverLayout.getMeasuredHeight();
                if (width < height)
                    height = width;
                else
                    width = height;
                ViewGroup.LayoutParams params = coverLayout.getLayoutParams();
                params.height = height;
                params.width = width;
                coverLayout.setLayoutParams(params);
                if (isPortraitOriented()) {
                    progressBar.setY(height - progressBar.getHeight() / 2);
                }

            }
        });

        playPauseButton = (ImageButton) findViewById(R.id.PlayPause);
        shuffleButton = (ImageButton) findViewById(R.id.shuffle_mode_btn);
        repeatButton = (ImageButton) findViewById(R.id.repeat_mode_btn);

        IntentFilter filter = new IntentFilter();
        filter.addAction(MusicService.NOTIFY_TRACK_CHANGED);
        filter.addAction(MusicService.NOTIFY_POSITION_CHANGED);
        filter.addAction(MusicService.NOTIFY_PLAYBACK_STATE_CHANGED);
        filter.addAction(MusicService.NOTIFY_EXIT);
        filter.addAction(MusicService.NOTIFY_SHOW_TOAST);
        filter.addAction(MusicService.NOTIFY_COVER_CHANGED);
        filter.addAction(MusicService.NOTIFY_PLAYING_MODE_CHANGED);
        filter.addAction(MusicService.NOTIFY_PLAYER_CLOSED);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, filter);
    }
    @Override
    public void onStart() {
        super.onStart();
        Controls.needControlPanelState();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * Sets an artist and a title of current track to relevant labels
     * @param artist An artist of the current track
     * @param title A title of the current track
     */
    private void setTitle(String artist, String title) {
        titleTextView.setText(title);
        artistTextView.setText(artist);
    }

    /**
     * Sets "Play/Pause" button image according to current playback state.
     * If state is playing button the image will be changed to pause icon,
     * if state is paused or any other the image it will be changed to play icon
     * @param state Current playback state of the player
     */
    private void SwitchButtonText (ControlPanelState.PlaybackState state) {
        switch (state) {
            case Playing:
                playPauseButton.setImageResource(R.drawable.pause_btn_selector);
               // playPauseButton.setText(getText(R.string.pause_btn_text));
                break;
            default:
                playPauseButton.setImageResource(R.drawable.play_btn_selector);
               // playPauseButton.setText(getText(R.string.play_btn_text));
                break;
        }
    }

    /**
     * Listener for "Play/Pause" button click. Disables button for 1s after click.
     * Stops incrementing of time position. Sends "playing control" message to a music service
     * @param v A view of "Play/Pause" button
     */
    public void OnPlayPauseClick(View v){
        playPauseButton.setClickable(false);
        Thread waiter = new PauseButtonWaiter();
        waiter.start();
        stopTimer();
        Controls.playControl();
    }

    /**
     * Listener for "Next track" button click. Sends "next track" message to a music service
     * @param v A view of "Next track" button
     */
    public void OnNextBtnClick(View v){
        Controls.nextControl();
    }

    /**
     * Listener for "Previous track" button click. Sends "previous track" message to a music service
     * @param v A view of "Previous track" button
     */
    public void OnPrevBtnClick(View v){
        Controls.previousControl();
    }

    /**
     * Listener for "Menu" button click. Also sets item click listener for a menu.
     * @param v A view of "Menu" button
     */
    public void OnMenuBtnClick(View v){
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_general, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.delete_track:
                        Controls.deleteTrack();
                        return true;
                    case R.id.action_settings:
                        return true;
                    case R.id.exit_app:
                        Intent musicServiceIntent = new Intent(MainActivity.this, MusicService.class);
                        stopService(musicServiceIntent);
                        finish();
                        return true;
                    default:
                        return false;
            }
        }});
        popup.show();
    }

    /**
     * Listener for "Show playlist" button click
     * @param v A view of "Show playlist" button
     */
    public void OnPlaylistBtnClick(View v){
        Intent a = new Intent(MainActivity.this, PlaylistActivity.class);
        startActivity(a);
    }

    /**
     * Listener for "Shuffling mode" button click
     * @param v A view of "Shuffling mode" button
     */
    public void OnShuffleBtnClick(View v){
        boolean activated = shuffleButton.isActivated();
        Controls.setShuffle(!activated);
    }

    /**
     * Listener for "Repeating mode" button click
     * @param v A view of "Repeating mode" button
     */
    public void OnRepeatBtnClick(View v){
        boolean activated = repeatButton.isActivated();
        Controls.setRepeat(!activated);
    }

    /**
     * Listens to progress bar changes.
     * Sets a chosen position time to the current time label.
     * Stops time incrementing while the user is making a touch gesture
     * Sends a new position to the player when the user has finished a touch gesture
     */
    private class ProgressListener implements SeekBar.OnSeekBarChangeListener {

       @Override
       public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
           timeTextView.post(new SetProgressStringTask(getTimeString(i)));
           //do nothing
       }

       @Override
       public void onStartTrackingTouch(SeekBar seekBar) {
           timing.cancel();
           timing.purge();

       }

       @Override
       public void onStopTrackingTouch(SeekBar seekBar) {
           int progress = seekBar.getProgress();
           Controls.positionControl(progress);
       }
    }

    /**
     * Timer task that increments progress bar position every second
     * and increment current time in a current position time label
     */
    class ProgressChanger extends TimerTask {

        @Override
        public void run() {
            int progress = progressBar.getProgress();
            int max = progressBar.getMax();
            progress = (progress < max) ? progress + 1 : max;
            progressBar.setProgress(progress);
            timeTextView.post(new SetProgressStringTask(getTimeString(progress)));
        }
    }


    /**
     * Task of setting a string iof a current position time to a current position time label
     */
    class SetProgressStringTask implements Runnable {

        private String currentProgress;

        /**
         * Constructs a task of setting a string of a new current position time
         * to a current position time label
         * @param progressString A string of a new position time
         */
        public SetProgressStringTask(String progressString) {
            this.currentProgress = progressString;
        }

        @Override
        public void run() {
            timeTextView.setText(currentProgress);
        }
    }

    /**
     * Task of waiting for a second after "Play/Pause" click
     * so that that the current playback state could finish changing before sending a new one
     */
    class PauseButtonWaiter extends Thread {

        @Override
        public void run() {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            playPauseButton.setClickable(true);
        }
    }

    /**
     * Gets a position from extras of an intent that was received with broadcast and sets it to the progress bar
     * and to the current position time label.
     */
    class updatePositionTask extends AsyncTask<Intent, Void, Integer> {

        @Override
        protected Integer doInBackground(Intent... params) {
            Intent intent = params[0];
            return intent.getExtras().getInt("position");
        }

        @Override
        protected void onPostExecute(Integer position) {
            super.onPostExecute(position);
            progressBar.setProgress(position);
            timeTextView.setText(getTimeString(position));
        }
    }

    /**
     * Task of getting a playing mode (shuffling on/off and repeating on/off)
     * from extras of an intent that was received with broadcast and sets "Shuffling mode" and "Repeating mode" buttons enabled
     * or disabled accordingly
     */
    class updatePlayingModeTask extends AsyncTask<Intent, Void, PlayingMode> {

        @Override
        protected PlayingMode doInBackground(Intent... params) {
            Intent intent = params[0];
            return intent.getExtras().getParcelable("mode");
        }

        @Override
        protected void onPostExecute(PlayingMode mode) {
            super.onPostExecute(mode);
            shuffleButton.setActivated(mode.isShuffling());
            repeatButton.setActivated(mode.isRepeating());
        }
    }


    /**
     * Task of getting a track info
     * from extras of an intent that was received with broadcast and sets it to relevant views.
     * Sends a message to a music service asking for an album cover of a current track. Cancels loading activity
     * if it is being shown
     */
    class updateTrackTask extends AsyncTask<Intent, Void, TrackInfoForUi> {
        Intent intent;
        @Override
        protected TrackInfoForUi doInBackground(Intent... params) {
            intent = params[0];

            return intent.getExtras().getParcelable("track_info");
        }

        @Override
        protected void onPostExecute(TrackInfoForUi info) {
            setInfoToUi(info);
            Controls.needCover();

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }

    /**
     * Task of getting a an album cover
     * from extras of an intent that was received with broadcast and sets it to album cover view.
     */
    class updateCoverTask extends AsyncTask<Intent, Void, Bitmap> {
        Intent intent;
        @Override
        protected Bitmap doInBackground(Intent... params) {
            intent = params[0];
            return intent.getExtras().getParcelable("cover");
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
             cover.setImageBitmap(bitmap);
        }

    }

    /**
     * Task of getting a new playback state from extras of an intent
     * that was received with broadcast. Sets relevant "Play/Pause" button icon
     * and also sets time incrementing on or off
     */
    class updatePlaybackTask extends AsyncTask<Intent, Void, ControlPanelState.PlaybackState> {

        @Override
        protected ControlPanelState.PlaybackState doInBackground(Intent... params) {
            Intent intent = params[0];
            ControlPanelState.PlaybackState playbackState =
                    (ControlPanelState.PlaybackState) intent.getExtras().get("state");
            return playbackState;
        }

        @Override
        protected void onPostExecute(ControlPanelState.PlaybackState state) {
            super.onPostExecute(state);
            setPlaybackState(state);
        }
    }

    /**
     * Switches "Play/Pause" button icon and turn time position incrementing on or off depending on a playback state value
     * @param playbackState Playback state
     */
    private void setPlaybackState(ControlPanelState.PlaybackState playbackState) {
        SwitchButtonText(playbackState);
        if (playbackState != null) {
            if (playbackState.equals(ControlPanelState.PlaybackState.Playing)) {
                restartTimer();
            } else {
                stopTimer();
            }
        }
    }

    /**
     * Stop time position incrementing
     */
    private synchronized void stopTimer() {
        timing.cancel();
        timing.purge();
    }

    /**
     * Restarts time position incrementing
     */
    private synchronized void restartTimer() {
        timing.cancel();
        timing.purge();
        timing = new Timer();
        timing.scheduleAtFixedRate(new ProgressChanger(), 0, 1000);
    }

    /**
     * Returns a string with formatted as "minutes:seconds" time position
     * @param timeValue Time position value in minutes
     * @return a string with formatted as "minutes:seconds" time position
     */
    public static String getTimeString(int timeValue) {

        String minutes = String.valueOf(timeValue / 60);
        int secondsValue = timeValue % 60;
        String seconds = String.valueOf(secondsValue);
        if (secondsValue < 10) {
            seconds = "0" + seconds;
        }

        return String.valueOf(minutes) + ":" + seconds;
    }

    /**
     * Sets an information about current track, that can be retrieved from extras of a broadcast message from music service
     * @param info track info
     */
    private void setInfoToUi(TrackInfoForUi info) {
        if (!info.isJustName()) {
            progressBar.setMax(info.getDuration());
            progressBar.setProgress(info.getPosition());
            setPlaybackState(info.getPlaybackState());
            timeTextView.setText(getTimeString(info.getPosition()));
            durationTextView.setText(getTimeString(info.getDuration()));
        }
        setTitle(info.getArtist(), info.getTitle());
    }

    /**
     * Prepares and shows an alert dialog when get broadcast message from music service about connection problems.
     * The dialog allows user to retry to connect to the server or to exit the application
     * @param url URL of current AIMP server
     */
    public void prepareClosedDialog(String url) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        OnRetryClickListener retryClickListener = new OnRetryClickListener(url);
        builder.setTitle("Connection error")
                .setMessage("Can't connect to the AIMP")
                .setCancelable(false)
                .setPositiveButton("Retry", retryClickListener)
                .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();

    }

    /**
     * Click listener for retry action in alert dialog about connection problems.
     * It restarts a music service with AIMP server URL
     */
    private class OnRetryClickListener implements DialogInterface.OnClickListener {
        private String url;

        /**
         * Constructs a retry action listener that restarts a music service with AIMP server URL
         * @param url AIMP server URL
         */
        public OnRetryClickListener(String url) {
            this.url = url;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            Intent musicServiceIntent = new Intent(MainActivity.this, MusicService.class);
            musicServiceIntent.putExtra("url", url);
            progressDialog.show();
            startService(musicServiceIntent);
        }
    }

    /**
     * Checks if a device screen is in a portrait mode now
     * @return true if a device screen is in a portrait mode now and false if it is in landscape mode
     */
    protected boolean isPortraitOriented () {
        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE))
                .getDefaultDisplay();

        int orientation = display.getRotation();

        return !(orientation == Surface.ROTATION_90
                || orientation == Surface.ROTATION_270);

    }

}
