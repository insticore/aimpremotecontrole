package insticore.com.aimpremotecontrol.receievers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.media.session.MediaButtonReceiver;
import android.view.KeyEvent;

import insticore.com.aimpremotecontrol.controls.Controls;

public class RemoteButtonReceiver extends MediaButtonReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_MEDIA_BUTTON)) {
            KeyEvent keyEvent = (KeyEvent) intent.getExtras().get(Intent.EXTRA_KEY_EVENT);
            if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                return;

            switch (keyEvent.getKeyCode()) {
                case KeyEvent.KEYCODE_HEADSETHOOK:
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    Controls.playControl();
                    break;
                case KeyEvent.KEYCODE_MEDIA_PLAY:
                    Controls.playControl();
                    break;
                case KeyEvent.KEYCODE_MEDIA_PAUSE:
                    Controls.playControl();
                    break;
                case KeyEvent.KEYCODE_MEDIA_STOP:
                   Controls.exit();
                    System.out.println("Stop service called");
                    break;
                case KeyEvent.KEYCODE_MEDIA_NEXT:
                    Controls.nextControl();
                    break;
                case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                    Controls.previousControl();
                    break;
            }
        }
    }
}
