package insticore.com.aimpremotecontrol.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import insticore.com.aimpremotecontrol.R;
import insticore.com.aimpremotecontrol.adapters.AddressListAdapter;
import insticore.com.aimpremotecontrol.containers.Address;
import insticore.com.aimpremotecontrol.services.MusicService;
import insticore.com.aimpremotecontrol.tools.IPScanner;
import insticore.com.aimpremotecontrol.controls.Handlers;
import insticore.com.aimpremotecontrol.tools.Tools;

public class ServerSetupActivity extends AppCompatActivity {

    private ListView listViewItems;
    private StartUpdatingList startUpdatingListTask;
    private ArrayList<Address> list;
    private ImageButton replaySearchButton;
    private ProgressBar progressBar;
    private Handler changeSearchButton;
    private Intent musicServiceIntent;


    private static final int MANUAL_CONNECTION_CALLED = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_search);
        if (Tools.isServiceRunning(MusicService.getServiceName(), getApplicationContext())) {
            Intent startActivityIntent = new Intent(ServerSetupActivity.this, MainActivity.class);
            new StartActivityTask(startActivityIntent).start();
            finish();
            return;
        }
        changeSearchButton = new android.os.Handler()  {
            @Override
            public void handleMessage(Message msg) {
                Boolean replay = (Boolean) msg.obj;
                if (replay) {
                    progressBar.setVisibility(View.INVISIBLE);
                    replaySearchButton.setVisibility(View.VISIBLE);
                    replaySearchButton.setClickable(true);

                } else {
                    replaySearchButton.setVisibility(View.INVISIBLE);
                    replaySearchButton.setClickable(false);
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        };
        replaySearchButton = (ImageButton) findViewById(R.id.replaySearchButton);
        progressBar = (ProgressBar) findViewById(R.id.searchProgressBar);
        listViewItems = (ListView) findViewById(R.id.servers_list_view);
        list = new ArrayList<>();
        final AddressListAdapter adapter = new AddressListAdapter(getApplicationContext(), list);
        Handlers.SERVERS_LIST_CHANGED_HANDLER = new android.os.Handler() {
            @Override
            public void handleMessage(Message msg) {
                adapter.notifyDataSetChanged();
            }
        };
        listViewItems.setAdapter(adapter);
        listViewItems.setOnItemClickListener(new OnListItemClick());
        changeSearchButton.sendMessage(changeSearchButton.obtainMessage(0, false));
        startUpdatingListTask = new StartUpdatingList();
        startUpdatingListTask.start();
    }
    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (startUpdatingListTask!=null && !startUpdatingListTask.isDone()) {
            startUpdatingListTask.setDone();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MANUAL_CONNECTION_CALLED) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    class OnListItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            startUpdatingListTask.setDone();
            Address add = (Address) listViewItems.getItemAtPosition(position);
            startMusicService(add.toString());
            Intent startActivityIntent = new Intent(ServerSetupActivity.this, MainActivity.class);
            new StartActivityTask(startActivityIntent).start();
            finish();
        }
    }

    /**
     * Listens for "Set manually" button click.
     * Opens an activity of manual setup, asking for result of this activity.
     * @param v A view of "Set manually" button
     */
    public void OnManualConnectClick(View v){
        Intent a = new Intent(ServerSetupActivity.this, ManualServerSetupActivity.class);
        startActivityForResult(a, MANUAL_CONNECTION_CALLED);
    }

    /**
     * Listens for "Replay" button click.
     * Starts an automatic search of AIMP server in local network and updating the list of found servers.
     * Unhides a progress bar.
     * @param v A view of "Replay" button
     */
    public void OnReplayClick(View v){
        startUpdatingListTask = new StartUpdatingList();
        startUpdatingListTask.start();
        changeSearchButton.sendMessage(changeSearchButton.obtainMessage(0, false));

    }

    /**
     * A task of automatic search of AIMP server in local network and updating the list of found servers
     */
    private class StartUpdatingList extends Thread {
        private IPScanner ipScanner;
        private boolean done = false;
        private Thread finishScanning = new Thread(new Runnable() {
            @Override
            public void run() {
                ipScanner.setDone();
            }
        });

        @Override
        public void run() {
            if(!isOnline()) {
                return;
            }
            ipScanner = new IPScanner(list);
            ipScanner.getOpenServers();
            changeSearchButton.sendMessage(changeSearchButton.obtainMessage(0, true));
            done = true;
        }

        /**
         * Sets ip scanner done, that makes all searching threads to finish and does not allow to start new searching threads
         */
        public void setDone() {
            done = true;
            finishScanning.start();
        }

        public boolean isDone() {
            return done;
        }
    }

    /**
     * Listens for "Cancel" button click.
     * Destroys this activity
     * @param v A view of "Cancel" button
     */
    public void onMainCancelButtonListener(View v) {
        finish();
    }

    /**
     * Starts a music service for AIMP server in a local network at provided URL address
     * @param url URL address of AIMP server in a local network
     */
    private void startMusicService(String url) {
        musicServiceIntent = new Intent(this, MusicService.class);
        musicServiceIntent.putExtra("url", url);
        new StartMusicService(musicServiceIntent).start();
    }

    /**
     * Checks if network connection is available on this device
     * @return true if network connection is available on this device
     */
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork.isConnectedOrConnecting();
    }

    /**
     * A task of starting a music service with provided intent
     */
    class StartMusicService extends Thread {
        private Intent intent;

        /**
         * Constructs a task of starting a music service with provided intent
         * @param intent An intent of starting a music service
         */
        public StartMusicService(Intent intent) {
            this.intent = intent;
        }

        @Override
        public void run() {
            startService(musicServiceIntent);
        }
    }

    /**
     * A task of starting an activity with provided intent
     */
    class StartActivityTask extends Thread {
        private Intent intent;

        /**
         * Creates a task of starting an activity with provided intent
         * @param intent
         */
        public StartActivityTask(Intent intent) {
            this.intent = intent;
        }

        @Override
        public void run() {
            startActivity(intent);
        }
    }

}

