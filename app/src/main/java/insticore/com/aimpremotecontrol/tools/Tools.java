package insticore.com.aimpremotecontrol.tools;

import android.app.ActivityManager;
import android.content.Context;

import java.net.MalformedURLException;
import java.net.URL;

import insticore.com.aimpremotecontrol.containers.Address;
import insticore.com.aimpremotecontrol.session.JSONSessionLight;

public class Tools {

    final public static String HTTP_PATH = "http://";

    final public static String RPC_PATH = "/RPC_JSON";

    final public static String IPADDRESS_PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    
    public static URL stringToURL(String strURL) {
        URL serverURL = null;
        try {
            serverURL = new URL(strURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return serverURL;
    }

    public static String getJsonRpcUrlString(String address) {
        return HTTP_PATH + address + RPC_PATH;
    }
    public static String addHttpToUrlString(String url) {
        printLine(url);
        String adress = HTTP_PATH;
        adress += url;
        return adress;
    }
    public static boolean isServiceRunning(String serviceName, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if(serviceName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean ipValidator(String text) {
        System.out.println(text);
        if (!text.matches (IPADDRESS_PATTERN)) {
            return false;
        } else {
            String[] splits = text.split("\\.");
            for (String split : splits) {
                if (Integer.valueOf(split) > 255) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void printLine(Object object) {
        System.out.println(object);
    }

    public static boolean checkPlugin(Address address) {
        return checkPlugin(address.toString());
    }

    public static boolean checkPlugin(String url) {
        JSONSessionLight session = new JSONSessionLight(url);
        boolean pluginIsWorking = false;
        session.getOptions().setConnectTimeout(20); // 25 milliseconds tested
        session.getOptions().setReadTimeout(100); //150 milliseconds tested
        for (int i = 2; i > 0; i--) {
            pluginIsWorking = session.checkPlugin();
            if (pluginIsWorking) break;
        }
        return pluginIsWorking;
    }
}
