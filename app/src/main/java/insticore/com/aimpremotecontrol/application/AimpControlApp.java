package insticore.com.aimpremotecontrol.application;

import android.app.Application;
import android.content.Context;

/**
 * Class that implements an application
 */
public class AimpControlApp extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    /**
     * Returns an application context
     * @return an application context
     */
    public static Context getContext() {
        return mContext;
    }

}
