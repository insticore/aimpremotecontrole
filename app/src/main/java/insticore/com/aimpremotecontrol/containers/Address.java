package insticore.com.aimpremotecontrol.containers;

public class Address {

    private String host;
    private String port;
    private String hostName;

    /**
     * Constructs an address with specified host address, port number and a host name
     * @param host a host address
     * @param port a port number
     * @param hostName a host name
     */
    public Address(String host, String port, String hostName) {
        this.host = host;
        this.port = port;
        this.hostName = hostName;
    }

    /**
     * Returns a host name
     * @return a host name
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Sets a host name
     * @param hostName A host name
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * Returns a host address
     * @return a host address
     */
    public String getHost() {
        return host;
    }

    /**
     * Sets a host address
     * @param host A host address
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Returns a port number
     * @return a port number
     */
    public String getPort() {
        return port;
    }

    /**
     * Sets  a port number
     * @param port A port number
     */
    public void setPort(String port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return getHost() + ":" + getPort();
    }
}
