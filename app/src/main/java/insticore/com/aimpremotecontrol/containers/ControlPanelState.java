package insticore.com.aimpremotecontrol.containers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import insticore.com.aimpremotecontrol.R;
import insticore.com.aimpremotecontrol.application.AimpControlApp;


public class ControlPanelState implements Parcelable {

    private Track currentTrack;
    private int playlistId;
    private int position;
    private PlaybackState playbackState;
    private Bitmap albumCover;
    private PlayingMode playingMode;

    /**
     * A default image for replacing missing album cover
     */
    public static Bitmap defaultAlbumCover = BitmapFactory.decodeResource(AimpControlApp.getContext().getResources(), R.drawable.aimp_transparent_cover);
    //public static Bitmap defaultAlbumCover = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888);

    /**
     * Playback states of a music player
     */
    public enum PlaybackState {
        Playing, Paused
    }

    /**
     * Constructs a control panel state with default values
     */
    public ControlPanelState() {
        currentTrack = new Track();
        playlistId = 0;
        position = 0;
        playbackState = PlaybackState.Paused;
        albumCover = ControlPanelState.defaultAlbumCover;
        playingMode = new PlayingMode();
    }

    /**
     * Returns a playlist id number
     * @return a playlist id number
     */
    public int getPlaylistId() {
        return playlistId;
    }

    /**
     * Sets a playlist id number
     * @param playlistId a playlist id number
     */
    public void setPlaylistId(int playlistId) {
        this.playlistId = playlistId;
    }

    /**
     * Returns a current position
     * @return a current position
     */
    public int getPosition() {
        return position;
    }

    /**
     * Sets a current position
     * @param position a current position
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * Returns a current playback state
     * @return a current playback state
     */
    public PlaybackState getPlaybackState() {
        return playbackState;
    }

    /**
     * Sets a current playback state
     * @param playbackState a current playback state
     */
    public void setPlaybackState(PlaybackState playbackState) {
        this.playbackState = playbackState;
    }

    /**
     * Returns an album cover image
     * @return an album cover image
     */
    public Bitmap getAlbumCover() {
        return albumCover;
    }

    /**
     * Sets an album cover image
     * @param albumCover an album cover image
     */
    public void setAlbumCover(Bitmap albumCover) {
        if (albumCover != null) {
            this.albumCover = albumCover;
        } else {
            this.albumCover = ControlPanelState.defaultAlbumCover;
        }
    }

    /**
     * Replaces a default album cover image
     * @param defaultAlbumCover new default album cover image
     */
    public static void setDefaultAlbumCover(Bitmap defaultAlbumCover) {
        ControlPanelState.defaultAlbumCover = defaultAlbumCover;
    }

    /**
     * Returns a current track
     * @return a current track
     */
    public Track getCurrentTrack() {
        return currentTrack;
    }

    /**
     * Sets a current track
     * @param currentTrack a current track
     */
    public void setCurrentTrack(Track currentTrack) {
        this.currentTrack = currentTrack;
    }

    /**
     * Returns a playing mode
     * @return a playing mode
     */
    public PlayingMode getPlayingMode() {
        return playingMode;
    }

    /**
     * Sets a playing mode
     * @param playingMode a playing mode
     */
    public void setPlayingMode(PlayingMode playingMode) {
        this.playingMode = playingMode;
    }

    /**
     * Constructs a control panel state from parsel
     */
    protected ControlPanelState(Parcel in) {
        currentTrack = (Track) in.readValue(Track.class.getClassLoader());
        playlistId = in.readInt();
        position = in.readInt();
        playbackState = (PlaybackState) in.readValue(PlaybackState.class.getClassLoader());
        albumCover = (Bitmap) in.readValue(Bitmap.class.getClassLoader());
        playingMode = (PlayingMode) in.readValue(PlayingMode.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(currentTrack);
        dest.writeInt(playlistId);
        dest.writeInt(position);
        dest.writeValue(playbackState);
        dest.writeValue(albumCover);
        dest.writeValue(playingMode);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ControlPanelState> CREATOR = new Parcelable.Creator<ControlPanelState>() {
        @Override
        public ControlPanelState createFromParcel(Parcel in) {
            return new ControlPanelState(in);
        }

        @Override
        public ControlPanelState[] newArray(int size) {
            return new ControlPanelState[size];
        }
    };
}