package insticore.com.aimpremotecontrol.session;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.util.ArrayList;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import insticore.com.aimpremotecontrol.tools.Tools;


public class JSONSessionLight extends JSONRPC2Session {
    protected String serverUrl;
    Random randomGenerator = new Random();

    public JSONSessionLight(String stringURL){
        super(Tools.stringToURL(Tools.getJsonRpcUrlString(stringURL)));
        this.serverUrl = stringURL;
    }

    public synchronized boolean checkPlugin() {
        String method = "Version";
        //HashMap<String, Object> result;
        try {
            List params = new ArrayList();
            params.add(new Object());
            int requestID = randomGenerator.nextInt(100000);
            JSONRPC2Request request = new JSONRPC2Request(method, params, requestID);
            // System.out.println(request.getID());
            try {
                send(request);
            } catch (JSONRPC2SessionException e) {
                throw new NoSuchElementException("Can't get JSON response: " + e.getMessage());
            }
            return true;
            //result = new ObjectMapper().readValue(response.getResult().toString(), HashMap.class);
            /*if (result != null) {
                System.out.println(result.get("aimp_version"));
                return true;
            }*/
        } catch (Exception e) {
            System.out.println(this.serverUrl + ": " + e.getMessage());
            return false;
        }
        //return false;
    }
}