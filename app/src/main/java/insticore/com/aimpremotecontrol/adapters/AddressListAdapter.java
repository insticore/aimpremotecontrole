package insticore.com.aimpremotecontrol.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import insticore.com.aimpremotecontrol.R;
import insticore.com.aimpremotecontrol.containers.Address;

/**
 * Custom adapter for ListView containing a list of found AIMP servers addresses.
 * Gets items from a List<Address> collection of found AIMP servers addresses.
 */
public class AddressListAdapter extends BaseAdapter {

    private List<Address> someList;
    private LayoutInflater layoutInflater;

    /**
     * Constructs an adapter for ListView containing a list of found AIMP servers addresses
     * based on a List<Address> collection
     * @param context An application context
     * @param someList A List<Address> collection of AIMP servers addresses
     */
    public AddressListAdapter(Context context, List<Address> someList) {
        this.someList = someList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return someList.size();
    }

    @Override
    public Object getItem(int position) {
        return someList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.server_row, parent, false);
        }
        Address address = getAddress(position);
        TextView bigText = (TextView) view.findViewById(R.id.host_name);
        bigText.setText(address.getHostName());
        TextView smallText = (TextView) view.findViewById(R.id.host_address);
        smallText.setText(address.toString());
        return view;
    }

    /**
     * Returns an address of a track with specified position
     * @param position
     * @return
     */
    private Address getAddress(int position) {
        return (Address) getItem(position);
    }
}
