package insticore.com.aimpremotecontrol.containers;

import android.os.Parcel;
import android.os.Parcelable;

public class Track implements Parcelable {

    private String title;
    private String artist;
    private int duration;
    private int id;
    private int playlistPosition;

    /**
     * Default text for empty artist value
     */
    public final static String UKNOWN_ARTIST = "Unknown artist";

    /**
     * Default text for empty title value
     */
    public final static String UKNOWN_TITLE = "Unknown title";

    /**
     * Constructs a track with default values
     */
    public Track() {
        id = 0;
        artist = UKNOWN_ARTIST;
        title = UKNOWN_TITLE;
        playlistPosition = 0;
    }

    /**
     * Constructs a playing mode with specified id, title, artist, duration and playlist position
     */
    public Track(int id, String title, String artist, int duration, int playlistPosition) {
        this.title = title;
        this.artist = artist;
        this.duration = duration;
        this.id = id;
        this.playlistPosition = playlistPosition;
    }

    /**
     * Returns a title of the track
     * @return a title of the track
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets a title of the track
     * @param title a title of the track
     */
    public void setTitle(String title) {
        if (title == null || title.isEmpty()) {
            this.title = UKNOWN_TITLE;
        } else {
            this.title = title;
        }
    }

    /**
     * Returns an artist of the track
     * @return an artist of the track
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Sets an artist of the track
     * @param artist An artist of the track
     */
    public void setArtist(String artist) {
        if (artist == null || artist.isEmpty()) {
            this.artist = UKNOWN_ARTIST;
        } else {
            this.artist = artist;
        }
    }

    /**
     * Returns a duration of the track
     * @return a duration of the track
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets a duration of the track
     * @param duration a duration of the track
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Returns the track id
     * @return the track id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the track id
     * @param id the track id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns a playlist position of the track
     * @return a playlist position of the track
     */
    public int getPlaylistPosition() {
        return playlistPosition;
    }

    /**
     * Sets a playlist position of the track
     * @param playlistPosition a playlist position of the track
     */
    public void setPlaylistPosition(int playlistPosition) {
        this.playlistPosition = playlistPosition;
    }

    /**
     * Constructs a track from parcel
     * @param in
     */
    protected Track(Parcel in) {
        title = in.readString();
        artist = in.readString();
        duration = in.readInt();
        id = in.readInt();
        playlistPosition = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeInt(duration);
        dest.writeInt(id);
        dest.writeInt(playlistPosition);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Track> CREATOR = new Parcelable.Creator<Track>() {
        @Override
        public Track createFromParcel(Parcel in) {
            return new Track(in);
        }

        @Override
        public Track[] newArray(int size) {
            return new Track[size];
        }
    };
}