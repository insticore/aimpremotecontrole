package insticore.com.aimpremotecontrol.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.LinkedHashMap;

import insticore.com.aimpremotecontrol.R;
import insticore.com.aimpremotecontrol.containers.Track;
import insticore.com.aimpremotecontrol.activities.MainActivity;

/**
 * Custom adapter for ListView containing a playlist.
 * Gets items from a HashMap<Integer, Track> collection of playlist tracks
 */
public class PlaylistAdapter extends BaseAdapter {

    private Track[] playlist;
    private LayoutInflater layoutInflater;

    /**
     * Creates an adapter for ListView containing a playlist
     * based on a HashMap<Integer, Track> collection of playlist tracks
     * @param context An application context
     * @param playlist A HashMap<Integer, Track> collection of playlist tracks
     */
    public PlaylistAdapter(Context context, HashMap<Integer, Track> playlist) {
        if (playlist == null || playlist.isEmpty()) {
            this.playlist = new Track[]{};
        } else {
            this.playlist = playlist.values().toArray(new Track[playlist.size()]);
        }
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return playlist.length;
    }

    @Override
    public Object getItem(int position) {
        return playlist[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.track_row, parent, false);
        }
        Track track = playlist[position];
        TextView titleText = (TextView) view.findViewById(R.id.playlist_track_title);
        titleText.setText(track.getTitle());
        TextView artistText = (TextView) view.findViewById(R.id.playlist_track_artist);
        TextView durationText = (TextView) view.findViewById(R.id.duration_in_playlist_textview);
        String artist = track.getArtist();
        artistText.setText(artist);
        durationText.setText(MainActivity.getTimeString(track.getDuration()));
        return view;
    }

    @Override
    public boolean isEmpty() {
        return playlist.length <= 0;
    }

    public void updateTracklist(LinkedHashMap<Integer, Track> playlist) {
        if (playlist == null || playlist.isEmpty()) {
            this.playlist = new Track[]{};
        } else {
            this.playlist = playlist.values().toArray(new Track[playlist.size()]);
        }
        notifyDataSetChanged();
    }
}
