package insticore.com.aimpremotecontrol.containers;

import android.os.Parcel;
import android.os.Parcelable;

public class TrackInfoForUi implements Parcelable {

    private String artist;
    private String title;
    private int duration;
    private int position;
    private ControlPanelState.PlaybackState playbackState;
    private boolean justName;

    /**
     * Constructs track info from parameters. Sets to update all info.
     * @param artist an artist of the track
     * @param title a title of the track
     * @param duration track duration
     * @param position playing position
     * @param playbackState a playback state
     */
    public TrackInfoForUi(String artist,
                          String title,
                          int duration,
                          int position,
                          ControlPanelState.PlaybackState playbackState) {
        this.artist = artist;
        this.title = title;
        this.duration = duration;
        this.position = position;
        this.playbackState = playbackState;
        justName = false;
    }

    /**
     * Constructs track info from artist and title parameters. Sets to update just name of the track.
     * @param artist
     * @param title
     */
    public TrackInfoForUi(String artist, String title) {
        this.artist = artist;
        this.title = title;
        justName = true;
    }

    /**
     * Returns a title of the track
     * @return a title of the track
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns an artist of the track
     * @return an artist of the track
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Returns a duration of the track
     * @return a duration of the track
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Returns a playing position
     * @return a playing position
     */
    public int getPosition() {
        return position;
    }

    /**
     * Returns playback state
     * @return playback state
     */
    public ControlPanelState.PlaybackState getPlaybackState() {
        return playbackState;
    }

    /**
     * Checks if all info needs to be updated or just track name
     * @return true if just track name needs to be updated
     */
    public boolean isJustName() {
        return justName;
    }

    /**
     * Constructs track info from parcel
     * @param in
     */
    protected TrackInfoForUi(Parcel in) {
        artist = in.readString();
        title = in.readString();
        duration = in.readInt();
        position = in.readInt();
        playbackState = (ControlPanelState.PlaybackState) in.readValue(ControlPanelState.PlaybackState.class.getClassLoader());
        justName = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(artist);
        dest.writeString(title);
        dest.writeInt(duration);
        dest.writeInt(position);
        dest.writeValue(playbackState);
        dest.writeByte((byte) (justName ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TrackInfoForUi> CREATOR = new Parcelable.Creator<TrackInfoForUi>() {
        @Override
        public TrackInfoForUi createFromParcel(Parcel in) {
            return new TrackInfoForUi(in);
        }

        @Override
        public TrackInfoForUi[] newArray(int size) {
            return new TrackInfoForUi[size];
        }
    };
}