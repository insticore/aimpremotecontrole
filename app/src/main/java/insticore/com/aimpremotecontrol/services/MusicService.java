package insticore.com.aimpremotecontrol.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.NotificationCompat;
import android.view.KeyEvent;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;

import java.util.NoSuchElementException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import insticore.com.aimpremotecontrol.containers.ControlPanelState;
import insticore.com.aimpremotecontrol.tools.MediaStyleHelper;
import insticore.com.aimpremotecontrol.R;
import insticore.com.aimpremotecontrol.containers.Track;
import insticore.com.aimpremotecontrol.activities.MainActivity;
import insticore.com.aimpremotecontrol.containers.TrackInfoForUi;
import insticore.com.aimpremotecontrol.controls.Controls;
import insticore.com.aimpremotecontrol.session.JSONSession;
import insticore.com.aimpremotecontrol.tools.OrderingExecutor;
import insticore.com.aimpremotecontrol.tools.Tools;

public class MusicService extends Service {

    private String serverAddress;

    JSONSession session;

    MyBinder binder = new MyBinder();

    private ExecutorService uiUpdatesExecutor = Executors.newSingleThreadExecutor();
    private ExecutorService listenCpsExecutor = Executors.newCachedThreadPool();
    private ExecutorService listenPlaylistExecutor = Executors.newCachedThreadPool();
    private OrderingExecutor orderingExecutor = new OrderingExecutor(uiUpdatesExecutor);

    Boolean done = false;
    public static final String NOTIFY_PREVIOUS = "insticore.com.aimpremotecontrol.previous";
    public static final String NOTIFY_EXIT = "insticore.com.aimpremotecontrol.exit";
    public static final String NOTIFY_PLAY_PAUSE = "insticore.com.aimpremotecontrol.playPause";
    public static final String NOTIFY_PLAY_TRACK = "insticore.com.aimpremotecontrol.playTrack";
    public static final String NOTIFY_NEXT = "insticore.com.aimpremotecontrol.next";
    public static final String NOTIFY_SHUFFLE = "insticore.com.aimpremotecontrol.shuffle";
    public static final String NOTIFY_REPEAT = "insticore.com.aimpremotecontrol.repeat";
    public static final String NOTIFY_TRACK_CHANGED = "insticore.com.aimpremotecontrol.track";
    public static final String NOTIFY_POSITION_CHANGED = "insticore.com.aimpremotecontrol.positionChanged";
    public static final String NOTIFY_PLAYBACK_STATE_CHANGED = "insticore.com.aimpremotecontrol.playbackStateChanged";
    public static final String NOTIFY_PLAYLIST_CHANGED = "insticore.com.aimpremotecontrol.playlist";
    public static final String NOTIFY_SHOW_TOAST = "insticore.com.aimpremotecontrol.toast";
    public static final String NOTIFY_POSITION = "insticore.com.aimpremotecontrol.position";
    public static final String NOTIFY_DELETE = "insticore.com.aimpremotecontrol.delete";
    public static final String NOTIFY_NEED_CPS = "insticore.com.aimpremotecontrol.needCps";
    public static final String NOTIFY_COVER_CHANGED = "insticore.com.aimpremotecontrol.cover";
    public static final String NOTIFY_NEED_COVER = "insticore.com.aimpremotecontrol.needCover";
    public static final String NOTIFY_PLAYING_MODE_CHANGED = "insticore.com.aimpremotecontrol.mode";
    public static final String NOTIFY_PLAYER_CLOSED = "insticore.com.aimpremotecontrol.closed";

    private int NOTIFICATION_ID = 1111;

    private MediaSessionCompat mediaSession;
    PlaybackStateCompat.Builder playbackStateBuilder;
    NotificationManager notificationManager;
    MediaMetadataCompat.Builder mediaMetadataBuilder;
    NotificationCompat.Builder notificationBuilder;
    public enum UpdateResult {
        Track, PlaybackState, Position, Playlist, PlayingMode, Name, Everything, Exit, Nothing
    }

    public enum NeedToUpdate {
        Nothing, TrackChanges, Everything, CpsChanges
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case MusicService.NOTIFY_PLAY_PAUSE:
                    if (session.getControlPanelState().getPlaybackState().equals(ControlPanelState.PlaybackState.Playing))
                        session.askToPause();
                    else
                        session.askToPlay();
                    break;
                case MusicService.NOTIFY_PLAY_TRACK:
                    Track track = intent.getExtras().getParcelable("track");
                    if (track != null) {
                        session.askToPlay(track.getId());
                    }
                    break;
                case MusicService.NOTIFY_NEXT:
                    session.askNextTrack();
                    break;
                case MusicService.NOTIFY_PREVIOUS:
                    session.askPreviousTrack();
                    break;
                case MusicService.NOTIFY_SHUFFLE:
                    boolean shuffle = intent.getExtras().getBoolean("shuffle");
                    session.setShuffling(shuffle);
                    break;
                case MusicService.NOTIFY_REPEAT:
                    boolean repeat = intent.getExtras().getBoolean("repeat");
                    session.setRepeating(repeat);
                    break;
                case MusicService.NOTIFY_DELETE:
                    Controls.showToast(session.deleteTrack());
                    break;
                case MusicService.NOTIFY_POSITION:
                    int position = intent.getExtras().getInt("position");
                    session.setPosition(position);
                    break;
                case MusicService.NOTIFY_NEED_CPS:
                    new GetControlPanelState(NeedToUpdate.Everything).start();
                    break;
                case MusicService.NOTIFY_NEED_COVER:
                    Controls.coverChanged(session.getControlPanelState().getAlbumCover());
                    break;
            }
        }
    };




    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        IntentFilter filter = new IntentFilter();
        filter.addAction(MusicService.NOTIFY_PLAY_PAUSE);
        filter.addAction(MusicService.NOTIFY_PLAY_TRACK);
        filter.addAction(MusicService.NOTIFY_NEXT);
        filter.addAction(MusicService.NOTIFY_PREVIOUS);
        filter.addAction(MusicService.NOTIFY_DELETE);
        filter.addAction(MusicService.NOTIFY_POSITION);
        filter.addAction(MusicService.NOTIFY_NEED_CPS);
        filter.addAction(MusicService.NOTIFY_NEED_COVER);
        filter.addAction(MusicService.NOTIFY_SHUFFLE);
        filter.addAction(MusicService.NOTIFY_REPEAT);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        serverAddress = intent.getExtras().getString("url");
        done = false;
        session = new JSONSession(serverAddress);
        JSONRPC2Response response = session.askPlayerControlPanelState();
        registerRemoteClient();
        updateUI(session.setControlPanelData(response, NeedToUpdate.Everything));
        session.getPlaylistEntries();
        listenCpsExecutor.submit(new ListenToCPChanges());
        listenPlaylistExecutor.submit(new ListenToTracklistChanges());


        return START_REDELIVER_INTENT;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        done = true;
        mediaSession.release();
        notificationManager.cancel(NOTIFICATION_ID);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return binder;
    }


    private class ListenToCPChanges implements Runnable {

        @Override
        public void run() {
            //ListenToCPChanges newListener = new ListenToCPChanges(session);

            JSONRPC2Response response = session.subscribeOnUpdateControlPanelState();
            //UpdateResult whatToUpdate = session.subscribeOnUpdateControlPanelState();
            if (!done) {
                listenCpsExecutor.submit(new ListenToCPChanges());
                //newListener.start();
            } else {
                return;
            }
/*
            UpdateResult whatToUpdate = session.setControlPanelData(response);
            updateUI(whatToUpdate);*/

           orderingExecutor.execute(new CpsUpdateTask(response, NeedToUpdate.CpsChanges), "CPSUpdate");
        }

    }

    private class ListenToTracklistChanges implements Runnable {
        @Override
        public void run() {
            boolean tracklistChanged = session.subscribeOnUpdatePlaylist();

            if(!done) {
                listenPlaylistExecutor.submit(new ListenToTracklistChanges());
            } else {
                return;
            }

            if (tracklistChanged) {
                new GetPlaylist().start();
                new GetControlPanelState(NeedToUpdate.TrackChanges).start();

            }
        }
    }



    private class GetControlPanelState extends Thread {
        NeedToUpdate whatToUpdate;


        public GetControlPanelState() {
            this.whatToUpdate = NeedToUpdate.Nothing;
        }

        public GetControlPanelState(NeedToUpdate whatToUpdate) {

            this.whatToUpdate = whatToUpdate;
        }
        @Override
        public void run() {
            //UpdateResult whatToUpdate = session.askPlayerControlPanelState();
            JSONRPC2Response response = session.askPlayerControlPanelState();

            orderingExecutor.execute(new CpsUpdateTask(response, whatToUpdate), "CPSUpdate");
        }
    }


    private class GetPlaylist extends Thread {

        @Override
        public void run() {
            try {
                session.getPlaylistEntries();
                updateUI(UpdateResult.Playlist);
            } catch (NoSuchElementException e) {
                e.printStackTrace();
            }
        }
    }

    public class MyBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    public JSONSession getSession() {
        return session;
    }

    static public String getServiceName() {
        return MusicService.class.getCanonicalName();
    }

    private void registerRemoteClient(){
        mediaSession = new MediaSessionCompat(getApplicationContext(), "MusicService");
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS
                | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        MediaSessionCompat.Callback callback = new MediaSessionCompat.Callback() {

            @Override
            public void onPlay() {
                Controls.playControl();
            }

            @Override
            public void onPause() {
                Controls.playControl();
            }

            @Override
            public void onSkipToNext() {
                Controls.nextControl();
            }

            @Override
            public void onSkipToPrevious() {
                Controls.previousControl();
            }

            @Override
            public void onStop() {
                System.out.println("Stop service called");
                Controls.exit();
                stopSelf();
            }

            @Override
            public void onSeekTo(long pos) {
                Controls.positionControl((int) pos / 1000);
            }
        };
        mediaSession.setCallback(callback);
        playbackStateBuilder = new PlaybackStateCompat.Builder();
        playbackStateBuilder.setActions(
                PlaybackStateCompat.ACTION_SEEK_TO |
                PlaybackStateCompat.ACTION_PLAY_PAUSE |
                PlaybackStateCompat.ACTION_SKIP_TO_NEXT |
                PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS |
                PlaybackStateCompat.ACTION_STOP);
        mediaSession.setActive(true);
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.requestAudioFocus(new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {
                // Ignore
            }
        }, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        mediaMetadataBuilder = new MediaMetadataCompat.Builder();
        mediaSession.setMetadata(mediaMetadataBuilder.build());
        notificationBuilder = new NotificationCompat.Builder(getApplicationContext());


        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        notificationBuilder.addAction(new NotificationCompat.Action(R.drawable.ic_stat_prev, "Prev", MediaStyleHelper.getActionIntent(this,KeyEvent.KEYCODE_MEDIA_PREVIOUS)));
        boolean playing = session.getControlPanelState().getPlaybackState().equals("playing");
        if (playing) {
            notificationBuilder.addAction(new NotificationCompat.Action(R.drawable.ic_stat_pause, "Pause", MediaStyleHelper.getActionIntent(this, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)));
        }
        else {
            notificationBuilder.addAction(new NotificationCompat.Action(R.drawable.ic_stat_play, "Play", MediaStyleHelper.getActionIntent(this, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)));
        }
        notificationBuilder.addAction(new NotificationCompat.Action(R.drawable.ic_stat_next, "Next", MediaStyleHelper.getActionIntent(this,KeyEvent.KEYCODE_MEDIA_NEXT)));
        notificationBuilder.setStyle(new NotificationCompat.MediaStyle()
                .setShowActionsInCompactView(0,1,2)
                .setShowCancelButton(true)
                .setCancelButtonIntent(MediaStyleHelper.getActionIntent(this,KeyEvent.KEYCODE_MEDIA_STOP))
                .setMediaSession(mediaSession.getSessionToken()))
                .setUsesChronometer(false)
                .setOngoing(true)
                .setWhen(0)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setShowWhen(false);

        Context context =  getApplicationContext();
        int requestID = (int) System.currentTimeMillis();
        Intent notificationIntent = new Intent(context, MainActivity.class);
         notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, requestID,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        notificationBuilder.setContentIntent(intent);
        notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    public void updateNotification(UpdateResult updateResult){
        boolean playing = session.getControlPanelState().getPlaybackState().equals(ControlPanelState.PlaybackState.Playing);
        int state = playing ? PlaybackStateCompat.STATE_PLAYING : PlaybackStateCompat.STATE_PAUSED;

        switch (updateResult) {
            case Everything:
            case Track:
                mediaMetadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ARTIST, session.getControlPanelState().getCurrentTrack().getArtist())
                        .putString(MediaMetadataCompat.METADATA_KEY_TITLE, session.getControlPanelState().getCurrentTrack().getTitle())
                        .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, session.getControlPanelState().getCurrentTrack().getDuration() * 1000)
                        .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, session.getControlPanelState().getAlbumCover());
            case Position:
            case PlaybackState:
                int position = session.getControlPanelState().getPosition() * 1000;
                playbackStateBuilder.setState(state, position, 1.0f);
                mediaSession.setPlaybackState(playbackStateBuilder.build());
                if (playing) {
                    notificationBuilder.mActions.set(1, new NotificationCompat.Action(R.drawable.ic_stat_pause, "Pause", MediaStyleHelper.getActionIntent(this, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)));
                }
                else {
                    notificationBuilder.mActions.set(1, new NotificationCompat.Action(R.drawable.ic_stat_play, "Play", MediaStyleHelper.getActionIntent(this, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)));
                }
                break;
            case Name:
                mediaMetadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ARTIST, session.getControlPanelState().getCurrentTrack().getArtist())
                        .putString(MediaMetadataCompat.METADATA_KEY_TITLE, session.getControlPanelState().getCurrentTrack().getTitle());
                break;
            default:
                //do nothing
                break;
        }
        mediaSession.setMetadata(mediaMetadataBuilder.build());
        updateNotificationBuilder(mediaSession);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    private void updateNotificationBuilder (MediaSessionCompat mediaSession) {
        MediaControllerCompat controller = mediaSession.getController();
        MediaMetadataCompat mediaMetadata = controller.getMetadata();
        MediaDescriptionCompat description = mediaMetadata.getDescription();

        notificationBuilder
                .setContentTitle(description.getTitle())
                .setContentText(description.getSubtitle())
                .setSubText(description.getDescription())
                .setLargeIcon(description.getIconBitmap())
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
    }

    public void updateUI (UpdateResult update) {
        switch (update) {
            case Exit:
                stopSelf();
                Controls.aimpClosed(serverAddress);
                break;

            case Position:
                int position = session.getControlPanelState().getPosition();
                Controls.positionChanged(position);
                Tools.printLine("Sent position " + position);
            case PlaybackState:
                Controls.playbackStateChanged(session.getControlPanelState().getPlaybackState());
                updateNotification(update);
                break;
            case PlayingMode:
                Controls.modeChanged(session.getControlPanelState().getPlayingMode());
                break;
            case Everything:
                Controls.modeChanged(session.getControlPanelState().getPlayingMode());
            case Track:
                TrackInfoForUi infoTrack = new TrackInfoForUi(
                        session.getControlPanelState().getCurrentTrack().getArtist(),
                        session.getControlPanelState().getCurrentTrack().getTitle(),
                        session.getControlPanelState().getCurrentTrack().getDuration(),
                        session.getControlPanelState().getPosition(),
                        session.getControlPanelState().getPlaybackState());
                Controls.trackChanged(infoTrack);
                updateNotification(update);
                break;
            case Name:
                TrackInfoForUi infoName = new TrackInfoForUi(
                        session.getControlPanelState().getCurrentTrack().getArtist(),
                        session.getControlPanelState().getCurrentTrack().getTitle());
                Controls.trackChanged(infoName);
                updateNotification(update);
                break;

            case Playlist:
                Controls.updatePlaylist(session.getPlaylist());
                break;

            default:
                //do nothing
                break;
        }
    }



    private class CpsUpdateTask implements Runnable {
        private JSONRPC2Response response;
        private NeedToUpdate whatToUpdate;

        public CpsUpdateTask(JSONRPC2Response response, NeedToUpdate whatToUpdate) {
            this.response = response;
            this.whatToUpdate = whatToUpdate;
        }

        @Override
        public void run() {
           updateUI(session.setControlPanelData(response, whatToUpdate));
        }
    }

}


